#! /usr/bin/env python3

# Adjust refs converted from SVN into a new, more consistent naming
# scheme, making some sanity checks in the process.

import argparse
import os.path
import re
import subprocess
import sys
import xml.etree.ElementTree


class GCCRepo:
    """GCC repository."""

    def __init__(self, gitrepo, svnrepo, this_script_dir):
        """Initialize a GCCRepo object."""
        self.gitrepo = gitrepo
        self.svnrepo = svnrepo
        self.svn_to_git = {}
        self.git_to_svn = {}
        branchify_list = []
        branchmap_list = []
        # Read gcc.opts to identify the mapping between git and SVN
        # branches.
        with open(os.path.join(this_script_dir, 'gcc.opts'), 'r') as f:
            for line in f:
                if line.startswith('#'):
                    continue
                line = line.strip()
                if line.startswith('branchify '):
                    branchify_list.extend(line[len('branchify '):].split())
                if line.startswith('branchmap '):
                    branchmap_list.extend(line[len('branchmap '):].split())
        branch_ignore = set()
        for b in branchify_list:
            if b.endswith('/*'):
                branch_ignore.add(b[:-len('/*')])
        svn_branch_list = []
        for b in branchify_list:
            if b.endswith('/*'):
                b_main = b[:-len('/*')]
                sub_list = self.svn_ls(b_main)
                sub_list = ['%s/%s' % (b_main, p) for p in sub_list]
                sub_list = [p for p in sub_list if p not in branch_ignore]
                svn_branch_list.extend(sub_list)
            else:
                svn_branch_list.append(b)
        for b in svn_branch_list:
            if b in self.svn_to_git:
                raise ValueError('duplicate SVN branch %s' % b)
            if b == 'trunk':
                self.svn_to_git[b] = 'refs/heads/master'
                continue
            found_map = False
            for m in branchmap_list:
                if not m.startswith(':'):
                    continue
                m = m[len(':'):]
                if not m.endswith(r'\1:'):
                    continue
                m = m[:-len(r'\1:')]
                mfrom, mto = m.split(':')
                if not mfrom.endswith('(.*)/'):
                    continue
                mfrom = mfrom[:-len('(.*)/')]
                if b.startswith(mfrom):
                    self.svn_to_git[b] = 'refs/%s%s' % (mto, b[len(mfrom):])
                    found_map = True
                    break
            if not found_map:
                raise ValueError('unmapped SVN branch %s' % b)
        for bs, bg in self.svn_to_git.items():
            if bg in self.git_to_svn:
                raise ValueError('duplicate git branch %s' % bg)
            self.git_to_svn[bg] = bs;
        self.git_ref_list = self.git_refs()

    @staticmethod
    def svn_command(args):
        """Run an SVN command, returning the output."""
        ret = subprocess.run(['svn'] + args, stdout=subprocess.PIPE,
                             check=True, universal_newlines=True)
        return ret.stdout

    def repo_uri(self, path):
        """Return SVN URI for a path."""
        return '%s/%s' % (self.svnrepo, path)

    def svn_ls(self, path):
        """List contents of a repository path."""
        ls_text = self.svn_command(['ls', self.repo_uri(path)])
        ls_list = ls_text.split()
        ls_list = [p.rstrip('/') for p in ls_list]
        return ls_list

    def svn_last_changed(self, path):
        """Return the last revision of a branch."""
        info_text = self.svn_command(['info', '--xml', self.repo_uri(path)])
        et = xml.etree.ElementTree.fromstring(info_text)
        return et.find('entry').find('commit').attrib['revision']

    def git_command(self, args):
        """Run a git command, returning the output."""
        ret = subprocess.run(['git'] + args, cwd=self.gitrepo,
                             stdout=subprocess.PIPE, check=True,
                             universal_newlines=True)
        return ret.stdout

    def git_refs(self):
        """Return the list of git references."""
        git_text = self.git_command(['for-each-ref', '--format=%(refname)'])
        return git_text.split()

    def git_move_ref(self, old_name, new_name):
        """Move a git ref to a new name."""
        self.git_command(['update-ref', new_name, old_name, ''])
        self.git_command(['update-ref', '-d', old_name])
        if old_name in self.git_to_svn:
            print('MAP: %s %s' % (self.git_to_svn[old_name], new_name))

    def git_move_to_namespace(self, old_name, namespace):
        """Move a git ref into a prefixed namespace."""
        if old_name.startswith('refs/'):
            new_name = 'refs/%s/%s' % (namespace,
                                       old_name[len('refs/'):])
        else:
            raise ValueError('bad ref name %s' % old_name)
        self.git_move_ref(old_name, new_name)

    def git_adjust_namespace(self, old_name, namespace_in, namespace_out):
        """Move a git ref into a prefixed namespace, adjusting existing use of
        a namespace name in the ref name."""
        if old_name.startswith('refs/heads/'):
            prefix = 'refs/heads/'
        elif old_name.startswith('refs/tags/'):
            prefix = 'refs/tags/'
        else:
            raise ValueError('bad ref name %s' % old_name)
        kind = prefix[len('refs/'):-1]
        main_name = old_name[len(prefix):]
        if (main_name.startswith('%s/' % namespace_in)
            or main_name.startswith('%s-' % namespace_in)
            or main_name.startswith('%s_' % namespace_in)):
            main_name = main_name[len(namespace_in)+1:]
        self.git_move_ref(old_name, 'refs/%s/%s/%s' % (namespace_out, kind,
                                                       main_name))

    def adjust_refs(self):
        """Adjust git refs into a preferred form."""
        for ref in self.git_ref_list:
            if ref.startswith('refs/deleted/'):
                # This is where reposurgeon places branches and tags
                # deleted in SVN.
                continue
            if ref not in self.git_to_svn:
                # Likely refs internally created by reposurgeon or
                # where it failed to delete a deleted ref.  Such cases
                # should be fixed (adjusting the expectations in this
                # script if necessary for special cases, or if
                # reposurgeon ends up deliberately creating
                # refs/deleted/ refs) so that no refs end up in
                # refs/UNEXPECTED/.
                self.git_move_to_namespace(ref, 'UNEXPECTED')
        git_ref_set = set(self.git_ref_list)
        for ref in self.svn_to_git.values():
            if ref not in git_ref_set:
                # Likely refs deleted via reposurgeon rather than in
                # SVN.  Such cases should be fixed by doing the
                # deletion in SVN.
                print('MISSING: %s' % ref)
        for ref in self.git_ref_list:
            if ref not in self.git_to_svn:
                continue
            if ref == 'refs/heads/master':
                print('MAP: %s %s' % (self.git_to_svn[ref], ref))
                continue
            # Move refs into appropriate namespaces.
            # Vendor refs.
            vendors = ('ARM', 'apple', 'csl', 'google', 'ibm', 'ix86',
                       'linaro', 'microblaze', 'redhat', 'rhl', 'st', 'suse',
                       'ubuntu')
            found_vendor = False
            for vendor in vendors:
                if (re.search('(?<![a-zA-Z])%s(?![a-zA-Z])' % vendor, ref)
                    and ref != 'refs/heads/%s' % vendor):
                    use_vendor = 'redhat' if vendor == 'rhl' else vendor
                    self.git_adjust_namespace(ref, vendor,
                                              'vendors/%s' % use_vendor)
                    found_vendor = True
                    break
            if found_vendor:
                continue
            # Branches marked dead in SVN.
            if ref.startswith('refs/heads/dead/'):
                self.git_adjust_namespace(ref, 'dead', 'dead')
                continue
            # Release branches.
            if re.fullmatch('refs/heads/(gcc|egcs|libgcj)[-_0-9]*branch', ref):
                branch_name = ref[len('refs/heads/'):-len('branch')]
                branch_name = branch_name.rstrip('-_')
                branch_name = branch_name.replace('-', '.')
                branch_name = branch_name.replace('_', '.')
                branch_name = branch_name.replace('.', '-', 1)
                # Avoid the 2.95.2.1 branch having the same name as
                # the release.
                if branch_name == 'gcc-2.95.2.1':
                    branch_name = 'gcc-2.95.2.1-branch'
                # Simplify the name of the EGCS 1.0 branch.
                branch_name = branch_name.replace('00', '0')
                new_name = 'refs/heads/releases/%s' % branch_name
                self.git_move_ref(ref, new_name)
                continue
            # Miscellaneous branches.  Presume dead if no commits
            # since December 2017.  Otherwise, presume an active
            # development branch.
            if ref.startswith('refs/heads/'):
                if int(self.svn_last_changed(self.git_to_svn[ref])) < 255286:
                    self.git_move_to_namespace(ref, 'dead')
                else:
                    branch_name = ref[len('refs/heads/'):]
                    new_name = 'refs/heads/devel/%s' % branch_name
                    self.git_move_ref(ref, new_name)
                    pass
                continue
            if not ref.startswith('refs/tags/'):
                raise ValueError('bad ref name %s' % ref)
            tag_name = ref[len('refs/tags/'):]
            if (re.fullmatch(
                    '(g77|gcc|egcs|libgcj)[-_0-9]*(release|RELEASE)?',
                    tag_name)
                # gcc-2_8 appears to be a prerelease version, with
                # gcc-2_8_0 as the actual release.
                and tag_name != 'gcc-2_8'):
                if tag_name.endswith('release'):
                    tag_name = tag_name[:-len('release')]
                if tag_name.endswith('RELEASE'):
                    tag_name = tag_name[:-len('RELEASE')]
                # g77 release tags appear to use a slightly different
                # name for the compiler (e.g. g77_0_5_22) and the
                # libraries (e.g. g77-0_5_22).
                if tag_name.startswith('g77-'):
                    tag_name = tag_name.replace('g77', 'libf2c')
                tag_name = tag_name.rstrip('-_')
                tag_name = tag_name.replace('-', '.')
                tag_name = tag_name.replace('_', '.')
                tag_name = tag_name.replace('.', '-', 1)
                if tag_name in ('egcs-1.0', 'egcs-1.1', 'gcc-2.95', 'gcc-3.0',
                                'gcc-3.1', 'gcc-3.2', 'gcc-3.3',
                                'libgcj-2.95'):
                    # Avoid ambiguity between branch and tag names.
                    tag_name = '%s.0' % tag_name
                new_name = 'refs/tags/releases/%s' % tag_name
                self.git_move_ref(ref, new_name)
                continue
            if re.match('LIBGCJ|egcs|gcc-|libstdc', tag_name):
                # Prerelease, release candidate and similar tags.
                tag_name = tag_name.replace('LIBGCJ', 'libgcj')
                tag_name = tag_name.replace('-', '.')
                tag_name = tag_name.replace('_', '.')
                tag_name = tag_name.replace('.', '-', 1)
                tag_name = tag_name.replace('.980309', '-980309')
                tag_name = tag_name.replace('.pre', '-pre')
                tag_name = tag_name.replace('prerelease.', 'prerelease-')
                tag_name = tag_name.replace('.rc', '-rc')
                tag_name = tag_name.replace('.test', '-test')
                new_name = 'refs/tags/prereleases/%s' % tag_name
                self.git_move_ref(ref, new_name)
                continue
            new_name = 'refs/tags/misc/%s' % tag_name
            self.git_move_ref(ref, new_name)


def main():
    """Main entry point."""
    parser = argparse.ArgumentParser(
        description='Rearrange GCC git conversion.')
    parser.add_argument('gitrepo', help='git repository location (local path)')
    parser.add_argument('svnrepo', help='SVN repository location')
    args = parser.parse_args()
    this_script_dir = os.path.abspath(os.path.dirname(sys.argv[0]))
    gcc_repo = GCCRepo(args.gitrepo, args.svnrepo, this_script_dir)
    gcc_repo.adjust_refs()


if __name__ == '__main__':
    main()
