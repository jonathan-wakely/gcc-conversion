# Makefile for gcc conversion using reposurgeon
#
# Steps to using this:
# 1. Make sure reposurgeon and repotool are on your $PATH.
# 2. (Skip this step if you're starting from a stream file.) For svn, set
#    REMOTE_URL to point at the remote repository you want to convert.
#    If the repository is already in a DVCS such as hg or git,
#    set REMOTE_URL to either the normal cloning URL (starting with hg://,
#    git://, etc.) or to the path of a local clone.
# 3. For cvs, set CVS_HOST to the repo hostname and CVS_MODULE to the module,
#    then uncomment the line that builds REMOTE_URL 
#    Note: for CVS hosts other than Sourceforge or Savannah you will need to 
#    include the path to the CVS modules directory after the hostname.
# 4. Set any required read options, such as --user-ignores or --nobranch,
#    by setting READ_OPTIONS.
# 5. Run 'make stubmap' to create a stub author map.
# 6. Run 'make' to build a converted repository.
#
# The reason both first- and second-stage stream files are generated is that,
# especially with Subversion, making the first-stage stream file is often
# painfully slow. By splitting the process, we lower the overhead of
# experiments with the lift script.
#
# For a production-quality conversion you will need to edit the map
# file and the lift script.  During the process you can set EXTRAS to
# name extra metadata such as a comments message-box.
#
# Afterwards, you can use the headcompare and tagscompare productions
# to check your work.
#

EXTRAS = bugdb.py fixbugmessages
REMOTE_URL = svn://gcc.gnu.org/svn/gcc
REPOSURGEON = reposurgeon
VERBOSITY = "set progress"
LOGFILE = conversion.log
READ_OPTIONS = --preserve --user-ignores --cvsignores
READLIMIT = 'readlimit 100'

SVNDUMP = gcc.svn

# Configuration ends here

.PHONY: local-clobber remote-clobber gitk gc compare clean dist stubmap
# Tell make not to auto-remove tag directories, because it only tries rm 
# and hence fails
.PRECIOUS: gcc-%-checkout gcc-%-git

default: gcc.git

gcc.git: gcc-git filters adjust-refs add-git-svn-history
	(cd gcc-git; rsync -a --delete . ../gcc.git)
	(cd gcc.git; ../filters) > filter.log 2>&1
	(d=`pwd`; \
	 cd gcc.git; \
	 ../adjust-refs . file://$${d}/gcc-mirror) > adjust.log 2>&1
	(cd gcc.git; ../add-git-svn-history) > history.log 2>&1
	cd gcc.git; \
	git \
	  -c gc.aggressiveWindow=1250 \
	  -c gc.aggressiveDepth=250 \
	  -c repack.writeBitmaps=true \
	  gc --aggressive

# Build the converted repo from the second-stage fast-import stream
gcc-git: gcc-fi
	rm -fr gcc-git
	$(REPOSURGEON) \
	    $(VERBOSITY) \
	    'read <gcc-fi' \
	    'prefer git' \
	    'rebuild gcc-git'

# Build the second-stage fast-import stream from the first-stage stream dump
gcc-fi: $(SVNDUMP) gcc.opts gcc.lift gcc.map $(EXTRAS)
	$(REPOSURGEON) \
	    $(VERBOSITY) \
	    'logfile $(LOGFILE)' \
	    $(READLIMIT) \
	    'script gcc.opts' \
	    "read $(READ_OPTIONS) <gcc.svn" \
	    'sourcetype svn' \
	    'prefer git' \
	    'script gcc.lift' \
	    'legacy write >gcc-fo' \
	    'write --legacy >gcc-fi'

# Build the first-stage stream dump from the local mirror
gcc.svn: gcc-mirror/db/current
	(cd gcc-mirror/ >/dev/null; repotool export) >gcc.svn

# Build a local mirror of the remote repository
gcc-mirror/db/current: mirror-update
	true

.PHONY: mirror-update
mirror-update:
	mkdir gcc-mirror 2> /dev/null || true
	(cd gcc-mirror && rsync --archive --delete --compress --progress rsync://gcc.gnu.org/gcc-svn .)
	echo "#!/bin/sh\nexit 0\n" >gcc-mirror/hooks/post-revprop-change
	chmod a+x gcc-mirror/hooks/post-revprop-change

# Make a local checkout of the source mirror for inspection
gcc-checkout: gcc-mirror
	cd gcc-mirror >/dev/null; repotool checkout $(PWD)/gcc-checkout

# Make a local checkout of the source mirror for inspection at a specific tag
gcc-%-checkout: gcc-mirror
	cd gcc-mirror >/dev/null; repotool checkout $(PWD)/gcc-$*-checkout $*

# Force rebuild of first-stage stream from the local mirror on the next make
local-clobber: clean
	rm -fr gcc-fi gcc-fo gcc-git *~ .rs* gcc-conversion.tar.gz gcc-*-git

# Force full rebuild from the remote repo on the next make.
remote-clobber: local-clobber
	rm -fr gcc.svn gcc-mirror gcc-checkout gcc-*-checkout

# Get the (empty) state of the author mapping from the first-stage stream
stubmap: gcc.svn
	$(REPOSURGEON) $(VERBOSITY) "read $(READ_OPTIONS) <gcc.svn" 'authors write >gcc.map'

# Compare the histories of the unconverted and converted repositories at head
# and all tags.
EXCLUDE = -x CVS -x .svn -x .git
EXCLUDE += -x .svnignore -x .gitignore
headcompare: gcc-mirror gcc-git
	repotool compare $(EXCLUDE) gcc-mirror gcc-git
tagscompare: gcc-mirror gcc-git
	repotool compare-tags $(EXCLUDE) gcc-mirror gcc-git
branchescompare: gcc-mirror gcc-git
	repotool compare-branches $(EXCLUDE) gcc-mirror gcc-git
allcompare: gcc-mirror gcc-git
	repotool compare-all $(EXCLUDE) gcc-mirror gcc-git

# General cleanup and utility
clean:
	rm -fr *~ .rs* gcc-conversion.tar.gz *.svn *-fi *-fo

# Bundle up the conversion metadata for shipping
SOURCES = Makefile gcc.lift gcc.map $(EXTRAS)
gcc-conversion.tar.gz: $(SOURCES)
	tar --dereference --transform 's:^:gcc-conversion/:' -czvf gcc-conversion.tar.gz $(SOURCES)

dist: gcc-conversion.tar.gz

#
# The following productions are git-specific
#

# Browse the generated git repository
gitk: gcc-git
	cd gcc-git; gitk --all

# Run a garbage-collect on the generated git repository.  Import doesn't.
# This repack call is the active part of gc --aggressive.  This call is
# tuned for very large repositories.
gc: gcc-git
	cd gcc-git; time git -c pack.threads=1 repack -AdF --window=1250 --depth=250
